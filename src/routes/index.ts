import express, { Express, Request, Response, Router, NextFunction } from 'express';
import redis, { createClient } from 'redis';
import middleware from '../modules/middleware';

import redisConnector from '../connections/redis';

var router: Router = express.Router()

const client = redisConnector.creaConnection();

router.use(middleware);

/* PUT Update user's cart */
router.put('/', async function(req: Request, res: Response, next: NextFunction) {
    console.log(req.body);
    const newCart = {
        last_update: new Date().getTime(),
        restaurant: req.body.restaurant,
        amount: req.body.amount,
        content: req.body.content
    }
    await client.connect();

    await client.set(req.body.user.id.toString(), JSON.stringify(newCart));

    await client.quit();

    res.json(newCart);
});

/* GET Get user's cart */
router.get('/', async function(req: Request, res: Response, next: NextFunction) {
    await client.connect();

    const cart = (await client.get(req.body.user.id.toString()))!
    
    await client.quit();

    if (cart == null) { 
        res.json({})
    } else {
        res.json(JSON.parse(cart));
    }
});

/* DELETE Empty user's cart */
router.delete('/', async function(req: Request, res: Response, next: NextFunction) {
    await client.connect();

    const reply: Number = (await client.del(req.body.user.id))!

    await client.quit();

    if(reply == 1) { res.json({message: reply}) }
    else { res.json({message: reply}) }
});


export default router;
