import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';

import indexRouter from './routes/index';

const app: Express = express();
const port: Number = 3001;

app.use(bodyParser.json());

app.use('/', indexRouter);
app.set('jwt', jwt);

app.listen(port, () => {
    console.log('[server]: The server is running at http://localhost:' + port);
});
