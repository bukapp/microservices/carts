import redis, { createClient } from 'redis';

export default {
    creaConnection: () => {
        const client = createClient({
            url: 'redis://default:' + process.env.REDIS_PASSWORD + '@' + process.env.REDIS_HOST + ':6379'
        });
        
        return client;
    }
}
